# FIBO-example


Here is an example of a minimal module corresponding to an example signature:

- ontology: **allloan_terminology.owl**
- signature: **signature_example**
- minimal module of the signature: **minimal module example**

Here **allloan_terminology.owl** is a sub-ontology of extract from **allloan.owl**, which belongs to the collection [FIBO (Financial Industry Business Ontology)](https://spec.edmcouncil.org/fibo/ontology/).

For simplicity, we replace the concepts and roles with their ids. For the real meaning of ids, see table **id2items.csv**.

A simple example of justification is provided in **Justification_example.md**.
