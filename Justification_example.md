# Example of justification
Over ontolgy "allloan_terminology". The justification of the conclusion (implies 1087 1009) is 

> \{(implies 1087 1051), (equivalent 1051 (and 046 1009))\}.

Where ids 1087, 1051, 1009 means:

- 1087: GovernmentSponsoredLoan
- 1051: Mortgage
- 1009: Loan
